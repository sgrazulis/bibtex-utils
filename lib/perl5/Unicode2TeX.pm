#------------------------------------------------------------------------
#$Author$
#$Date$ 
#$Revision$
#$URL$
#------------------------------------------------------------------------

package Unicode2TeX;

use strict;
use Unicode::Normalize;
## use utf8;
## use charnames ':full';

require Exporter;
@Unicode2TeX::ISA = qw(Exporter);
@Unicode2TeX::EXPORT = qw(unicode2tex tex2unicode);

my %commands = (
#
# Arrows and mathematical symbols:
#
    "\x{2190}" => '\\leftarrow',  # LEFTWARDS ARROW
    "\x{2192}" => '\\rightarrow', # RIGHTWARDS ARROW
    "\x{00D7}" => '\\times',      # MULTIPLICATION SIGN (times)
    "\x{2012}" => '--',           # EN DASH             (dash) (?)
    "\x{2013}" => '--',           # EN DASH             (dash)
    "\x{2014}" => '---',          # EM DASH             (single bond)
    "\x{00B1}" => '\\pm',         # PLUS-MINUS SIGN     (plus-minus)
    "\x{2213}" => '-+',           # MINUS-OR-PLUS SIGN  (minus-plus)
    ## "\x{003D}" => '\\db',      # EQUALS SIGN         (double bond)
    ## "\x{2A75}" => '\\db',      # TWO EQUALS SIGNS    (used for double bond)
    "\x{25A1}" => '\\square',     # WHITE SQUARE        (square)
    "\x{2261}" => '\\tb',         # IDENTICAL TO        (triple bond)
    "\x{2260}" => '\\neq',        # NOT EQUAL TO

    # For this symbol, no suitable Unicode character could be found.
    # '\\ddb' delocalized double bond

    "\x{223C}" => '\\sim',    # TILDE OPERATOR
    "\x{2329}" => '\\langle', # LEFT-POINTING ANGLE BRACKET     (langle)
    "\x{232A}" => '\\rangle', # RIGHT-POINTING ANGLE BRACKET     (rangle)
    "\x{2243}" => '\\simeq',  # ASYMPTOTICALLY EQUAL TO
    "\x{221E}" => '\\infty',  # INFINITY

    "\x{00B7}\x{00B7}\x{00B7}" => '\\cdots',   # three MIDDLE DOTs
    "\x{00B7}" => '\\cdot',    # MIDDLE DOT
);


#
# %alt_cmd is used only to transform from Unicode to TEX commands. For
# back translation only main forms, %commands, are used, since
# information about the variety of Unicode characters is lost.
#

my %alt_cmd = (
#
# Alternative Unicode encoding of some commands:
#
    # Main:
    # "\x{232A}" => '\\rangle', # RIGHT-POINTING ANGLE BRACKET     (rangle)
    #
    # Alternatives:
    #">"       => '\\rangle',   # GREATER-THAN SIGN
    "\x{27E9}" => '\\rangle',   # MATHEMATICAL RIGHT ANGLE BRACKET (rangle)
    "\x{3009}" => '\\rangle',   # RIGHT ANGLE BRACKET              (rangle)
    # "\x{203A}" => '\\rangle', # SINGLE RIGHT-POINTING ANGLE QUOTATION

    # Main:
    # "\x{2329}" => '\\langle ', # LEFT-POINTING ANGLE BRACKET     (langle)
    #
    # Alternatives:
    #"<"       => '\\langle',   # LESS-THAN SIGN
    "\x{27E8}" => '\\langle',   # MATHEMATICAL LEFT ANGLE BRACKET (langle)
    "\x{3008}" => '\\langle',   # LEFT ANGLE BRACKET              (langle)
    # "\x{2039}" => '\\langle', # SINGLE LEFT-POINTING ANGLE QUOTATION

    # Main:
    # "\x{223C}" => '\\sim', # TILDE OPERATOR
    #
    # Alternatives:
    "\x{02DC}" => '\\sim',   # SMALL TILDE
    "\x{2053}" => '\\sim',   # SWUNG DASH
    "\x{FF5E}" => '\\sim',   # FULLWIDTH TILDE
    # character '~' (TILDE) is not used since it denotes subscript in TEX.

    # Main: "\x{25A1}" => '\\square', # WHITE SQUARE        (square)
    # "\x{2610}" => '\\square',       # BALLOT BOX          (square) (?)
    #
    # 'Ballot box' character, though similar to 'square', seems inappropriate
    # to denote mathematical and chemical formulae and therefore
    # is at present not interpreted.
    #
);

my %greek = (
#
# Greek letters:
#
    "\x{03B1}" => '\alpha',    # alpha
    "\x{03B2}" => '\beta',     # beta
    "\x{03B3}" => '\gamma',    # gamma
    "\x{03B4}" => '\delta',    # delta
    "\x{03B5}" => '\epsilon',  # epsilon
    "\x{03B5}" => '\varepsilon', # varepsilon
    "\x{03B6}" => '\zeta',     # zeta
    "\x{03B7}" => '\eta',      # eta
    "\x{03B8}" => '\theta',    # theta
    "\x{03B9}" => '\iota',     # iota
    "\x{03BA}" => '\kappa',    # kappa
    "\x{03BB}" => '\lambda',   # lambda
    "\x{03BC}" => '\miu',      # miu
    "\x{03BC}" => '\mu',       # miu
    "\x{03BD}" => '\niu',      # niu
    "\x{03BD}" => '\nu',       # niu
    "\x{03BE}" => '\ksi',      # ksi
    "\x{03BF}" => '\omicron',  # omicron
    "\x{03C0}" => '\pi',       # pi
    "\x{03C1}" => '\rho',      # rho
    "\x{03C2}" => '\varsigma', # varsigma
    "\x{03C3}" => '\sigma',    # sigma
    "\x{03C4}" => '\tau',      # tau
    "\x{03C5}" => '\upsilon',  # upsilon
    "\x{03C6}" => '\phi',      # phi
    "\x{03C6}" => '\varphi',   # phi
    "\x{03C7}" => '\chi',      # chi
    "\x{03C8}" => '\psi',      # psi
    "\x{03C9}" => '\omega',    # omega
);

my %alt_letters = (
    "\x{00EF}" => '\"\i',    # LATIN SMALL LETTER I WITH DIARESIS,
    "\x{00CF}" => '\"\I',    # LATIN CAPITAL LETTER I WITH DIARESIS,
                             # sometimes typesetted as "dotless i with diaresis"
);

my %letters = (
#
# Some special European letters
#
    "\x{00DF}" => '\ss',     # LATIN SMALL LETTER SHARP S (German eszett)
    "\x{00B0}" => '\degree', # DEGREE SIGN
    "\x{0141}" => '\L',      # LATIN CAPITAL LETTER L WITH STROKE
    "\x{0142}" => '\l',      # LATIN SMALL LETTER L WITH STROKE
    "\x{00D8}" => '\O',      # LATIN CAPITAL LETTER O WITH STROKE
    "\x{00F8}" => '\o',      # LATIN SMALL LETTER O WITH STROKE
    "\x{0131}" => '\i',      # LATIN SMALL LETTER DOTLESS I
    "\x{0237}" => '\j',      # LATIN SMALL LETTER DOTLESS J

    ## "\x{0110}" => '\DJ',     # LATIN CAPITAL LETTER D WITH STROKE (barred D)
    ## "\x{0111}" => '\dj',     # LATIN SMALL LETTER D WITH STROKE (barred d)
    "\x{00D0}" => '\DJ',     # LATIN CAPITAL LETTER ETH
    "\x{00F0}" => '\dj',     # LATIN SMALL LETTER  ETH

    ## "A\x{030A}" => '\AA',    # LATIN CAPITAL LETER A with ring above
    ## "a\x{030A}" => '\aa',    # LATIN SMALL LETER A with ring above
    ## "\x{212B}"  => '\AA',    # angstrom sign
    "\x{00C5}" => '\AA',     # LATIN CAPITAL LETER A WITH RING ABOVE
    "\x{00E5}" => '\aa',     # LATIN SMALL LETER A WITH RING ABOVE

    "\x{00C6}" => '\AE',     # LATIN CAPITAL LETTER AE
    "\x{00E6}" => '\ae',     # LATIN SMALL LETTER AE
    "\x{0152}" => '\OE',     # LATIN CAPITAL LIGATURE OE
    "\x{0153}" => '\oe',     # LATIN SMALL LIGATURE OE
);

my %combining = (
#
# Combining diacritical marks:
#

   # Signs declared to work in TeX in the "Voshititel'nyj
   # TeX" and experimentaly know to work in LaTeX:

   "\x{0300}" => '\`',   #   COMBINING GRAVE ACCENT (grave)
   "\x{0301}" => '\\\'', #'# COMBINING ACUTE ACCENT (acute)
   "\x{0302}" => '\^',   #   COMBINING CIRCUMFLEX ACCENT (circumflex)
   "\x{0308}" => '\"',   #   COMBINING DIARESIS     (umlaut)
   "\x{0303}" => '\~',   #   COMBINING TILDE        (tilde)

   # Signs experimentaly know to work in LaTeX:

   "\x{0304}" => '\=',   #   COMBINING MACRON       (overbar)
   "\x{0307}" => '\.',   #   COMBINING DOT ABOVE    (overdot)

   # Signs declared to work in TeX in the "Voshititel'nyj
   # TeX" and experimentaly know to work in LaTeX:

   "\x{030B}" => '\H',   #   COMBINING DOUBLE ACUTE ACCENT (Hungarian umlaut)
   
   "\x{0306}" => '\u',   #   COMBINING BREVE (breve)
   "\x{030C}" => '\v',   #   COMBINING CARON        (hacek)
   "\x{0331}" => '\b',   #   COMBINING (underbar)
   "\x{0323}" => '\d',   #   COMBINING (dodt below)
   "\x{0327}" => '\c',   #   COMBINING CEDILLA      (cedilla)

   # Signs experimentaly know to work in LaTeX:

   "\x{0328}" => '\k',   #   COMBINING OGONEK       (ogonek)

   # The Czech strange character found in the IUCr BibTeX file
   # ka1009.bibtex as of 2009.04.20:

   "\x{030A}" => '\accent23' # Combining ring above, according to:
    # http://ftp.fi.muni.cz/pub/localization/charsets/cs-encodings-faq
    # accessed on 2009.04.20 9:26 Vilnius time
); 

my %combining_alt = (
#
# Combining diacritical marks, alternatives:
#

   # Signs declared to work in TeX in the "Voshititel'nyj TeX", but
   # not confirmed in LaTeX:

   "\x{0304}" => '\B',   #   COMBINING MACRON       (overbar)
   "\x{0307}" => '\D',   #   COMBINING DOT ABOVE    (overdot)
);

#
# Add upper-case Greek letters:
#

for my $i ( 0x0391 .. 0x03A9 ) {
    if( $i != 0x03A2 ) { # reserved code-point, could be an uppercase varsigma
	my $c = chr($i);
	my $letter_name = $greek{lc($c)};
	$letter_name =~ s/^\\//;
	$greek{$c} = "\\" . ucfirst($letter_name);
	## binmode(STDOUT,":utf8");
	## print ">>> $c, $tex{$c}\n";
    }
}

my %letter_codes = ();

my %tex = ( %commands, %alt_cmd, %letters, %greek );

sub unicode2tex
{
    my $text = Unicode::Normalize::normalize( 'D', $_[0] );

    for my $pattern (keys %tex) {
	$text =~ s/$pattern/$tex{$pattern}/g;
    }
    for my $pattern (keys %combining) {
	$text =~ s/(.)($pattern)/$2$1/g;
	$text =~ s/$pattern/$combining{$pattern}/g;
    }
    $text =~ s/([^\x{0000}-\x{007F}])/sprintf("&#x%04X;",ord($1))/eg;
    return $text;
}

while( my ($unicode,$tex) = each %letters ) {
    $letter_codes{$tex} = $unicode;
}

sub tex2unicode
{
    my $text = $_[0];

    # Force $text encoding into UTF8:
    $text = pack( "U*", unpack("C*", $text));

    # Convert IUCr-specific markup (bugs?) into the mainstream LaTeX:

    $text =~ s/\{\\'\\\}(.)\{\}/{\\' $1}/g;
    $text =~ s/\{\\"\\\}(.)\{\}/{\\" $1}/g;

    # Convert escaped TeX markup characters into Unicode private code
    # points:

    $text =~ s/\\\$/\x{E000}/g;
    $text =~ s/\\\{/\x{E001}/g;
    $text =~ s/\\\}/\x{E002}/g;
    $text =~ s/\\\\/\x{E003}/g;

    $text =~ s/\{\\-\}//g;
    $text =~ s/\\-//g;
    $text =~ s/\{\$\^\\prime\$\}/'/g; #'
    $text =~ s/\^\\prime/'/g; #'
    ## $text =~ s/\{\\sb\s+(.*?)\}/<sub>$1<\/sub>/g;
    ## $text =~ s/\{\\sp\s+(.*?)\}/<sup>$1<\/sup>/g;
    $text =~ s/\{\\sb\s+(.*?)\}/~$1~/g;
    $text =~ s/\{\\sp\s+(.*?)\}/^$1^/g;
    $text =~ s/\{(?:\\it\s*\\bf|\\bf\s*\\it)\s(.*?)\}/<b><i>$1<\/i><\/b>/g;
    $text =~ s/\{\\it\s(.*?)\}/<i>$1<\/i>/g;
    $text =~ s/\{\\bf\s(.*?)\}/<b>$1<\/b>/g;
    $text =~ s/\{\\sc\s(.*?)\}/"<small>".uc($1)."<\/small>"/eg;
    $text =~ s/\{--\}/--/g;

    ## $text =~ s/\\\\db /\x{003D}/g;
    $text =~ s/\{\\'\\(i|j)\}/$1\x{0301}/g;
    for my $pattern (sort {$b cmp $a} keys %commands) {
	my $value = $commands{$pattern};
    	$text =~ s/\{\$\Q$value\E\$\}/$pattern/g;
    	$text =~ s/\{\Q$value\E\}/$pattern/g;
    	$text =~ s/\Q$value\E/$pattern/g;
    }
    for my $pattern (sort {$b cmp $a} keys %greek) {
	$text =~ s/\{\Q$greek{$pattern}\E\}\{\}/$pattern/g;
	$text =~ s/\{\Q$greek{$pattern}\E\}/$pattern/g;
	$text =~ s/\{\$\Q$greek{$pattern}\E\$\}/$pattern/g;
	$text =~ s/\$\Q$greek{$pattern}\E\$/$pattern/g;
	$text =~ s/\Q$greek{$pattern}\E\{\}/$pattern/g;
	$text =~ s/\Q$greek{$pattern}\E/$pattern/g;
    }
    for my $pattern (sort {$b cmp $a} keys %alt_letters) {
	$text =~ s/\Q$alt_letters{$pattern}\{\}\E/$pattern/g;
	$text =~ s/\{\Q$alt_letters{$pattern}\E\}\{\}/$pattern/g;
	$text =~ s/\{\Q$alt_letters{$pattern}\E\}/$pattern/g;
	$text =~ s/\{\$\Q$alt_letters{$pattern}\E\$\}/$pattern/g;
	$text =~ s/\$\Q$alt_letters{$pattern}\E\$/$pattern/g;
	$text =~ s/\Q$alt_letters{$pattern}\E\{\}/$pattern/g;
	$text =~ s/\Q$alt_letters{$pattern}\E/$pattern/g;
    }
    for my $pattern (sort {$b cmp $a} keys %letters) {
	$text =~ s/\Q$letters{$pattern}\{\}\E/$pattern/g;
	$text =~ s/\{\Q$letters{$pattern}\E\}\{\}/$pattern/g;
	$text =~ s/\{\Q$letters{$pattern}\E\}/$pattern/g;
	$text =~ s/\{\$\Q$letters{$pattern}\E\$\}/$pattern/g;
	$text =~ s/\$\Q$letters{$pattern}\E\$/$pattern/g;
	$text =~ s/\Q$letters{$pattern}\E\{\}/$pattern/g;
	$text =~ s/\Q$letters{$pattern}\E/$pattern/g;
    }
    for my $pattern (sort {$b cmp $a} keys %combining) {
	$text =~ s/\{(\Q$combining{$pattern}\E)\s*(.)\}/$2$pattern/g;
	$text =~ s/\{(\Q$combining{$pattern}\E)\s*\{(.)\}\}/$2$pattern/g;
	$text =~ s/(\Q$combining{$pattern}\E)\s*(.)\{\}/$2$pattern/g;
	$text =~ s/(\Q$combining{$pattern}\E)\s*\{(.)\}/$2$pattern/g;
	$text =~ s/(\Q$combining{$pattern}\E)\s*(.)/$2$pattern/g;
    }
    for my $pattern (sort {$b cmp $a} keys %combining_alt) {
	$text =~ s/\{(\Q$combining_alt{$pattern}\E)\s*(.)\}/$2$pattern/g;
	$text =~ s/\{(\Q$combining_alt{$pattern}\E)\s*\{(.)\}\}/$2$pattern/g;
	$text =~ s/(\Q$combining_alt{$pattern}\E)(.)\{\}/$2$pattern/g;
	$text =~ s/(\Q$combining_alt{$pattern}\E)\{(.)\}/$2$pattern/g;
	$text =~ s/(\Q$combining_alt{$pattern}\E)(.)/$2$pattern/g;
    }

    $text =~ s/\$//g;

    $text =~ s/\&\#x([0-9A-Fa-f]+);/chr(hex($1))/eg;

    # Covert the previously encoded escaped TeX characters back into
    # their original form:

    $text =~ s/\x{E000}/\$/g;
    $text =~ s/\x{E001}/\{/g;
    $text =~ s/\x{E002}/\}/g;
    $text =~ s/\x{E003}/\\/g;

    return Unicode::Normalize::normalize( 'C', $text );
}

return 1;
